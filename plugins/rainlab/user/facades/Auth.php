<?php namespace RainLab\User\Facades;

use October\Rain\Support\Facade;
use Session;
use DB;

class Auth extends Facade
{
    /**
     * Get the registered name of the component.
     * @return string
     */
    protected static function getFacadeAccessor() { return 'user.auth'; }

    public static function get_user_id()
    {
        $data = Session::all();
        if(isset($data['user_auth']))
        {
            return $data['user_auth']['0'];
        }
        else
        {
            return false;
        }
    }
    
    public static function get_user_group()
    {
    	$data = Session::all();
    	$user_id = $data['user_auth']['0'];
    	$group_id = DB::select('select * from users_groups where user_id = ?', [$user_id]);
    	return $group_id;
    }
}
