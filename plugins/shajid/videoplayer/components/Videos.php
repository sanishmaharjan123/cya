<?php namespace Shajid\Videoplayer\Components;

use Cms\Classes\ComponentBase;
use Lang;
use Cms\Classes\Page;

use Shajid\Videoplayer\Classes\ComponentHelper;
use SystemException;
use Exception;

class Videos extends ComponentBase
{
	 /**
     * A collection of records to display
     * @var \October\Rain\Database\Collection
     */
    public $records;

    /**
     * Message to display when there are no records.
     * @var string
     */
    public $noRecordsMessage;

    /**
     * Reference to the page name for linking to details.
     * @var string
     */
    public $detailsPage;

    /**
     * Specifies the current page number.
     * @var integer
     */
    public $pageNumber;

    /**
     * Parameter to use for the page number
     * @var string
     */
    public $pageParam;

    /**
     * Model column name to display in the list.
     * @var string
     */
    public $displayColumn; 

    /**
     * Model column to use as a record identifier in the details page links
     * @var string
     */
    public $detailsKeyColumn;

    /**
     * Name of the details page URL parameter which takes the record identifier.
     * @var string
     */
    public $detailsUrlParameter;

   

    //
    // Properties
    //
    public function componentDetails()
    {
        return [
            'name'        => 'videos Component',
            'description' => 'Displays all your uploaded videos',
			'icon' => 'icon-play-circle'
        ];
    }

    public function defineProperties()
    {
        return [
		 'modelClass' => [
                'title'       => 'Model Class',
                'type'        => 'dropdown',
                'showExternalParam' => false
            ],
            
            'displayColumn' => [
                'title'       => 'Display Column',
                'description' => 'shajid.videoplayer::lang.components.list_display_column_description',
                'type'        => 'autocomplete',
                'depends'     => ['modelClass'],
                'validation'  => [
                    'required' => [
                        'message' => Lang::get('shajid.videoplayer::lang.components.list_display_column_required')
                    ]
                ]
            ],
            'noRecordsMessage' => [
                'title'        => 'No records message',
                'description'  => 'No Records Found',
                'type'         => 'string',
                'default'      => 'No Records Found',
                'showExternalParam' => false,
            ],
           
           
            
            'recordsPerPage' => [
                'title'             => 'RecordsPerPage',
                'description'       => 'Pagination',
                'type'              => 'string',
                'validationPattern' => '^[0-9]*$',
                'validationMessage' => 'shajid.videoplayer::lang.components.list_records_per_page_validation',
                'group'             => 'Pagination'
            ],
            'pageNumber' => [
                'title'       => 'pageNumber',
                'description' => 'Pagination',
                'type'        => 'string',
                'default'     => '{{ :page }}',
                'group'       => 'Pagination'
            ],
            'sortColumn' => [
                'title'       => 'sortColumn',
                'description' => 'shajid.videoplayer::lang.components.list_sort_column_description',
                'type'        => 'autocomplete',
                'depends'     => ['modelClass'],
                'group'       => 'sorting',
                'showExternalParam' => false
            ],
            'sortDirection' => [
                'title'       => 'Direction',
                'type'        => 'dropdown',
                'showExternalParam' => false,
                'group'       => 'sorting',
                'options'     => [
                    'asc'     => 'shajid.videoplayer::lang.components.list_order_direction_asc',
                    'desc'    => 'shajid.videoplayer::lang.components.list_order_direction_desc'
                ]
            ]
		
];
    }
	
	 public function getDetailsPageOptions()
    {
        $pages = Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');

        $pages = [
            '-' => Lang::get('shajid.builder::lang.components.list_details_page_no')
        ] + $pages;

        return $pages;
    }

    public function getModelClassOptions()
    {
        return ComponentHelper::instance()->listGlobalModels();
    }

    public function getDisplayColumnOptions()
    {
        return ComponentHelper::instance()->listModelColumnNames();
    }

    public function getDetailsKeyColumnOptions()
    {
        return ComponentHelper::instance()->listModelColumnNames();
    }

    public function getSortColumnOptions()
    {
        return ComponentHelper::instance()->listModelColumnNames();
    }

    public function getScopeOptions()
    {
        $modelClass = ComponentHelper::instance()->getModelClassDesignTime();

        $result = [
            '-' => Lang::get('shajid.videoplayer::lang.components.list_scope_default')
        ];
        try {
            $methods = get_class_methods($modelClass);

            foreach ($methods as $method) {
                if (preg_match('/scope[A-Z].*/', $method)) {
                    $result[$method] = $method;
                }
            }
        }
        catch (Exception $ex) {
            // Ignore invalid models
        }

        return $result;
    }

    //
    // Rendering and processing
    //

    public function onRun()
    {
        $this->prepareVars();

        $this->records = $this->page['records'] = $this->listRecords();
    }

    protected function prepareVars()
    {
        $this->noRecordsMessage = $this->page['noRecordsMessage'] = Lang::get($this->property('noRecordsMessage'));
        $this->displayColumn = $this->page['displayColumn'] = $this->property('displayColumn');
        $this->pageParam = $this->page['pageParam'] = $this->paramName('pageNumber');

        $this->detailsKeyColumn = $this->page['detailsKeyColumn'] = $this->property('detailsKeyColumn');
        $this->detailsUrlParameter = $this->page['detailsUrlParameter'] = $this->property('detailsUrlParameter');

        $detailsPage = $this->property('detailsPage');
        if ($detailsPage == '-') {
            $detailsPage = null;
        }

        $this->detailsPage = $this->page['detailsPage'] = $detailsPage;

        if (!strlen($this->displayColumn)) {
            throw new SystemException('The display column name is not set.');
        }

        if (strlen($this->detailsPage)) {
            if (!strlen($this->detailsKeyColumn)) {
                throw new SystemException('The details key column should be set to generate links to the details page.');
            }

            if (!strlen($this->detailsUrlParameter)) {
                throw new SystemException('The details page URL parameter name should be set to generate links to the details page.');
            }
        }
    }

    protected function listRecords()
    {
        $modelClassName = $this->property('modelClass');
        if (!strlen($modelClassName) || !class_exists($modelClassName)) {
            throw new SystemException('Invalid model class name');
        }

        $model = new $modelClassName();
        $scope = $this->getScopeName($model);

        if ($scope !== null) {
            $model = $model->$scope();
        }

        $model = $this->sort($model);
        $records = $this->paginate($model);

        return $records;
    }

    protected function getScopeName($model)
    {
        $scopeMethod = trim($this->property('scope'));
        if (!strlen($scopeMethod) || $scopeMethod == '-') {
            return null;
        }

        if (!preg_match('/scope[A-Z].+/', $scopeMethod)) {
            throw new SystemException('Invalid scope method name.');
        }

        if (!method_exists($model, $scopeMethod)) {
            throw new SystemException('Scope method not found.');
        }

        return lcfirst(substr($scopeMethod, 5));
    }

    protected function paginate($model)
    {
        $recordsPerPage = trim($this->property('recordsPerPage'));
        if (!strlen($recordsPerPage)) {
            // Pagination is disabled - return all records
            return $model->get();
        }

        if (!preg_match('/^[0-9]+$/', $recordsPerPage)) {
            throw new SystemException('Invalid records per page value.');
        }

        $pageNumber = trim($this->property('pageNumber'));
        if (!strlen($pageNumber) || !preg_match('/^[0-9]+$/', $pageNumber)) {
            $pageNumber = 1;
        }

        return $model->paginate($recordsPerPage, $pageNumber);
    }

    protected function sort($model)
    {
        $sortColumn = trim($this->property('sortColumn'));
        if (!strlen($sortColumn)) {
            return $model;
        }

        $sortDirection = trim($this->property('sortDirection'));

        if ($sortDirection !== 'desc') {
            $sortDirection = 'asc';
        }

        // Note - no further validation of the sort column
        // value is performed here, relying to the ORM sanitizing.
        return $model->orderBy($sortColumn, $sortDirection);
    }
	
	
}
