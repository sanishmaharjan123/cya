<?php namespace Shajid\Videoplayer\Models;

use Model;


/**
 * Model
 */
class Video extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'shajid_videoplayer_';
	
	
	public $attachOne =
	[
	'video' => 'System\Models\File'
	];
}