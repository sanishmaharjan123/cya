<?php namespace Shajid\Videoplayer;

use Backend;
use Controller;

use System\Classes\PluginBase;
use Event;







use Shajid\Videoplayer\Models\Video;



class Plugin extends PluginBase
{
	
	public function pluginDetails()
    {
        return [
            'name' => 'Video Plugin',
            'description' => 'See you uploaded videos.',
            'author' => 'Shajid planet',
			'icon'        => 'icon-play-circle',
            
        ];
    }

    public function registerComponents()
    {
		 return [
            'Shajid\Videoplayer\Components\Videos' => 'Video'
        ];
    }

   
	 public function registerNavigation()
    {
        return [
            'videoplayer' => [
                'label'       => 'shajid.videoplayer::lang.plugin.name',
                'url'         => Backend::url('shajid/videoplayer/videos'),
                'icon'        => 'icon-youtube-play',
               
                'permissions' => ['shajid.videoplayer.manage_plugins'],
                'order'       => 400,

                'sideMenu' => [
                    'database' => [
                        'label'       => 'shajid.videoplayer::lang.database.menu_label',
                        'icon'        => 'icon-hdd-o',
                        'url'         => 'javascript:;',
                        'attributes'  => ['data-menu-item'=>'database'],
                        'permissions' => ['shajid.videoplayer.manage_plugins']
                    ],
                    'models' => [
                        'label'       => 'shajid.videoplayer::lang.model.menu_label',
                        'icon'        => 'icon-random',
                        'url'         => 'javascript:;',
                        'attributes'  => ['data-menu-item'=>'models'],
                        'permissions' => ['rshajid.videoplayer.manage_plugins']
                    ],
                    'permissions' => [
                        'label'       => 'shajid.videoplayer::lang.permission.menu_label',
                        'icon'        => 'icon-unlock-alt',
                        'url'         => '#',
                        'attributes'  => ['data-no-side-panel'=>'true', 'data-builder-command'=>'permission:cmdOpenPermissions', 'data-menu-item'=>'permissions'],
                        'permissions' => ['shajid.videoplayer.manage_plugins']
                    ],
                    'menus' => [
                        'label'       => 'shajid.videoplayer::lang.menu.menu_label',
                        'icon'        => 'icon-location-arrow',
                        'url'         => 'javascript:;',
                        'attributes'  => ['data-no-side-panel'=>'true', 'data-builder-command'=>'menus:cmdOpenMenus', 'data-menu-item'=>'menus'],
                        'permissions' => ['shajid.videoplayer.manage_plugins']
                    ],
                    'controllers' => [
                        'label'       => 'shajid.videoplayer::lang.controller.menu_label',
                        'icon'        => 'icon-asterisk',
                        'url'         => 'javascript:;',
                        'attributes'  => ['data-menu-item'=>'controllers'],
                        'permissions' => ['shajid.videoplayer.manage_plugins']
                    ],
                    'versions' => [
                        'label'       => 'shajid.videoplayer::lang.version.menu_label',
                        'icon'        => 'icon-code-fork',
                        'url'         => 'javascript:;',
                        'attributes'  => ['data-menu-item'=>'version'],
                        'permissions' => ['shajid.videoplayer.manage_plugins']
                    ],
                    'localization' => [
                        'label'       => 'shajid.videoplayer::lang.localization.menu_label',
                        'icon'        => 'icon-globe',
                        'url'         => 'javascript:;',
                        'attributes'  => ['data-menu-item'=>'localization'],
                        'permissions' => ['shajid.videoplayer.manage_plugins']
                    ]
                ]

            ]
        ];
    }
}
