<?php namespace Shajid\Videoplayer\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateShajidVideoplayer extends Migration
{
    public function up()
    {
        Schema::create('shajid_videoplayer_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('shajid_videoplayer_');
    }
}
