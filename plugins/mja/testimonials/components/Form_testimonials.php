<?php 

	namespace Mja\Testimonials\Components;

	use Cms\Classes\ComponentBase;
	use Mja\Testimonials\Models\Testimonial;
	use Rainlab\User\Models\User;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\Input;
	use Redirect;
	class Form_testimonials extends ComponentBase
	{
		public $testimonial_form;
		public function componentDetails()
		{
			return [
				'name' => 'Testimonial From Form',
				'description' => 'Add testimonial from front end'		
			];
		}

		public function onRun()
	    {
	    	

	        $this->page['testimonials'] = $this->testimonials = Testimonial::whereIsPublic(1)->get();
	    }

	    public function onSave()
	    {
	    	$user = Auth::get_user_id();
	    	$user_all = User::where('id',1)->first();
	    	$username = $user_all->first_name;
	    	$testimonial = new Testimonial;
	    	$testimonial->content = Input::get('content');
	    	$testimonial->photo = Input::file('photo');
	    	$testimonial->author_bio = Input::get('author_bio');
	    	$testimonial->author = $username;
	    	$testimonial->date = date('Y-m-d');
	    	$testimonial->is_public =1;
	    	$testimonial->save();	
	    	return Redirect::to('/testimonial-page');
	    	// dd($testimonial);

	    }
	}