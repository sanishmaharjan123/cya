<?php namespace cya\Offices\Components;


use Input;
use Mail;
use cya\Offices\models\Contact;

class ContactForm extends \Cms\Classes\ComponentBase
{

	public function componentDetails()
	{
		return [
			'name' => 'Contact Form',
			'description' => ' Simple contact form'
		];
	}

	public function onSend()
	{
		$contact = new contact;
		$contact->name = Input::get('name');
		$contact->email = Input::get('email');
		$contact->message = Input::get('message');
		$contact->save();

		$vars = ['name' => Input::get('name'), 'email'=> Input::get('email'), 'content' => Input::get('message')];
		Mail::send('cya.contact::mail.message',$vars,function($message)
		{
			$message->to('anish@pagodalabs.com','Admin Panel');
			$message->subject('New message from contact form');

		});
	}


}

