<?php namespace cya\Offices\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaOffices extends Migration
{
    public function up()
    {
        Schema::create('cya_offices_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('name', 255);
            $table->string('phone_no', 255);
            $table->string('address', 255);
            $table->string('city', 255);
            $table->string('state', 255);
            $table->string('zip_code', 255)->nullable();
            $table->string('notes', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_offices_');
    }
}
