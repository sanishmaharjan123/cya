<?php namespace Cya\Clientdetails\Models;

use Model;
use DB;


/**
 * Model
 */
class Gps_trial_model extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'cya_clientdetails_gps_trial';

    public static function get_gps_details($user_id)
    {
        return DB::table('cya_clientdetails_gps_trial')->where('user_id',$user_id)->get();
    }

    public static function get_user_credentials($id)
    {
        return DB::table('users')->where('id',$id)->first();
    }
}
