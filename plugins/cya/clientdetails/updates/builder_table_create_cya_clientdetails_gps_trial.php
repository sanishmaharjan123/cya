<?php namespace Cya\Clientdetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaClientdetailsGpsTrial extends Migration
{
    public function up()
    {
        Schema::create('cya_clientdetails_gps_trial', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('user_id');
            $table->string('client_name', 255);
            $table->date('client_dob');
            $table->string('client_email', 255);
            $table->string('client_mobile', 255);
            $table->string('client_home', 255);
            $table->text('client_revolving_account');
            $table->text('client_installment');
            $table->text('client_mortgage');
            $table->text('client_expenses');
            $table->text('client_income');
            $table->text('client_bank_info');
            $table->integer('has_spouse');
            $table->string('spouse_name', 255);
            $table->string('spouse_dob', 255);
            $table->string('spouse_email', 255);
            $table->string('spouse_mobile', 255);
            $table->string('spouse_home', 255);
            $table->text('spouse_revolving_account');
            $table->text('spouse_installment');
            $table->text('spouse_mortgage');
            $table->text('spouse_expenses');
            $table->text('spouse_income');
            $table->text('spouse_bank_info');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_clientdetails_gps_trial');
    }
}
