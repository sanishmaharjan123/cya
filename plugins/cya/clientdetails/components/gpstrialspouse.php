<?php namespace cya\clientdetails\Components;

use Input;

use cya\clientdetails\models\Gps_trial_model;

use Illuminate\Support\Facades\Auth;

use Validator;

use Redirect;

use RainLab\User\Components\Session;



class gpstrialspouse extends \Cms\Classes\ComponentBase
{

	public function componentDetails()
	{
		return [
			'name' => 'Client Spouse  Details',
			'description' => ' Clinet Spouse Details Form'
		];
	}

	public function onSave_spouse()
	{
		
		// save basic information
		$spouse_information = Gps_trial_model::where('user_id', Auth::get_user_id())->first();
		$spouse_information->spouse_name = Input::get('spouse_name');
		$spouse_information->spouse_dob = Input::get('spouse_dob');
		$spouse_information->spouse_email = Input::get('spouse_email');
		$spouse_information->spouse_mobile = Input::get('spouse_mobile_no');
		$spouse_information->spouse_home = Input::get('spouse_second_no');
		$spouse_information->has_spouse = 1;
		$spouse_information->save();
	}

	public function onSave_spouse_revolving()
	{
		// save revolving accounts
		$count_rev_count = count(Input::get('spouse_rev_account_balance'));
		$rev_account_balance = Input::get('spouse_rev_account_balance');
		$rev_interest_rate = Input::get('spouse_rev_interest_rate');
		$rev_minimum_payment = Input::get('spouse_rev_minimum_payment');
		$rev_actual_pay = Input::get('spouse_rev_actual_pay');
		$rev_due_date = Input::get('spouse_rev_due_date');
		$rev_spouse_id = Input::get('spouse_rev_spouse_id');
		for ($i=0; $i < $count_rev_count; $i++) 
		{ 
			$revolving_account[$i] = [
				'rev_account_balance'=> $rev_account_balance[$i],
				'rev_interest_rate'=> $rev_interest_rate[$i],
				'rev_minimum_payment'=> $rev_minimum_payment[$i],
				'rev_actual_pay'=> $rev_actual_pay[$i],
				'rev_due_date'=> $rev_due_date[$i]
			];
		}

		$gps = Gps_trial_model::where('user_id', Auth::get_user_id())->first();
		$gps->spouse_revolving_account = (json_encode($revolving_account));
		$gps->save();

	}

	public function onSave_spouse_installment()
	{
			// save installment section
		$inst_installment_account = Input::get('spouse_inst_installment_account');
		$inst_interest_rate = Input::get('spouse_inst_interest_rate');
		$inst_payment = Input::get('spouse_inst_payment');
		$inst_due_date = Input::get('spouse_inst_due_date');

		$count_installment = count($inst_installment_account);
		for ($i=0; $i < $count_installment; $i++) 
		{ 
			$installment[$i] = [
				'inst_installment_account'=> $inst_installment_account[$i],
				'inst_interest_rate'=> $inst_interest_rate[$i],
				'inst_payment'=> $inst_payment[$i],
				'inst_due_date'=> $inst_due_date[$i]
			];
		}
		$gps = Gps_trial_model::where('user_id', Auth::get_user_id())->first();
		$gps->spouse_installment = (json_encode($installment));
		$gps->save();
	}

	public function onSave_spouse_mortgage()
	{

			// save mortgage
		$mor_mortgage_type = Input::get('spouse_mor_mortgage_type');
		$mor_payment = Input::get('spouse_mor_payment');
		$mor_interest_rate = Input::get('spouse_mor_interest_rate');
		$mor_balance = Input::get('spouse_mor_balance');
		$mor_purchase_price = Input::get('spouse_mor_purchase_price');
		$mor_estimated_value = Input::get('spouse_mor_estimated_value');

		$count_mortgage = count($mor_mortgage_type);
		for ($i=0; $i < $count_mortgage; $i++) 
		{ 
			$mortgage[$i] = [
				'mor_mortgage_type'=> $mor_mortgage_type[$i],
				'mor_payment'=> $mor_payment[$i],
				'mor_interest_rate'=> $mor_interest_rate[$i],
				'mor_balance'=> $mor_balance[$i],
				'mor_purchase_price'=> $mor_purchase_price[$i],
				'mor_estimated_value'=> $mor_estimated_value[$i]
			];
		}
		$gps = Gps_trial_model::where('user_id', Auth::get_user_id())->first();
		$gps->spouse_mortgage = (json_encode($mortgage));
		$gps->save();

	}
	public function onSave_spouse_expenses()
	{
			// save expense
		$inhouse_expense_name = Input::get('spouse_inhouse_expense_name');
		$inhouse_other_expenses = Input::get('spouse_inhouse_other_expenses');

		$overall_expense_name = Input::get('spouse_overall_expense_name');
		$overall_other_expenses = Input::get('spouse_overall_other_expenses');

		$count_inhouse = count($inhouse_expense_name);
		for ($i=0; $i < $count_inhouse; $i++) 
		{ 
			$inhouse_expense[$i] = [
				'inhouse_expense_name'=> $inhouse_expense_name[$i],
				'inhouse_other_expenses'=> $inhouse_other_expenses[$i]
			];
		}

		$count_overall = count($overall_expense_name);
		for ($i=0; $i < $count_overall; $i++) 
		{ 
			$overall_expense[$i] = [
				'overall_expense_name'=> $overall_expense_name[$i],
				'overall_other_expenses'=> $overall_other_expenses[$i],
			];
		}

		$expense = [
			'exp_groceries' => Input::get('spouse_inhouse_groceries'),
			'exp_cable' => Input::get('spouse_inhouse_cable'),
			'exp_cellphone' => Input::get('spouse_inhouse_cellphone'),
			'exp_utilities' => Input::get('spouse_inhouse_utilities'),
			'exp_clothes' => Input::get('spouse_inhouse_clothes'),
			'exp_dry_cleaning' => Input::get('spouse_inhouse_dry_cleaning'),
			'exp_entertainment' => Input::get('spouse_inhouse_entertainment'),
			'exp_emergency' => Input::get('spouse_inhouse_emergency'),
			'inhouse_other_expenses' => json_encode($inhouse_expense),
			'inhouse_total' => Input::get('spouse_inhouse_total'),
			'exp_grooming' => Input::get('spouse_overall_grooming'),
			'exp_tolls' => Input::get('spouse_overall_tolls'),
			'exp_eating_out' => Input::get('spouse_overall_eating_out'),
			'exp_gas' => Input::get('spouse_overall_gas'),
			'exp_children_lesso' => Input::get('spouse_overall_children_lesso'),
			'exp_daycare' => Input::get('spouse_overall_daycare'),
			'exp_hair_nails' => Input::get('spouse_overall_hair_nails'),
			'exp_gym' => Input::get('spouse_overall_gym'),
			'overall_expenses' => json_encode($overall_expense),
			'overall_total' => Input::get('spouse_overall_total')
		];

		$gps = Gps_trial_model::where('user_id', Auth::get_user_id())->first();
		$gps->spouse_expenses = (json_encode($expense));
		$gps->save();
	}
	public function onSave_spouse_income()
	{		
			// save income
		$inc_pay_frequency = Input::get('spouse_inc_pay_frequency');
		$inc_net_income = Input::get('spouse_inc_net_income');
		$inc_extra_income = Input::get('spouse_inc_extra_income');
		$inc_pay_day = Input::get('spouse_inc_pay_day');

		$count_income = count($inc_pay_frequency);
		for ($i=0; $i < $count_income; $i++) 
		{ 
			$income[$i] = [
				'inc_pay_frequency'=> $inc_pay_frequency[$i],
				'inc_net_income'=> $inc_net_income[$i],
				'inc_extra_income'=> $inc_extra_income[$i],
				'inc_pay_day'=> $inc_pay_day[$i],
			];
		}
		$gps = Gps_trial_model::where('user_id', Auth::get_user_id())->first();
		$gps->spouse_income = (json_encode($income));
		$gps->save();
	}

	public function onSave_spouse_bank_info()
	{
			// save bank info
		$bank_info = [
			'bank_checking_account_balance' => Input::get('spouse_bank_checking_account_balance'),
			'bank_saving_account_balance' => Input::get('spouse_bank_saving_account_balance'),
			'bank_pretax_saving_plan' => Input::get('spouse_bank_pretax_saving_plan'),
			'bank_contribution_per_day' => Input::get('spouse_bank_contribution_per_day'),
			'bank_get_match' => Input::get('spouse_bank_get_match'),
			'bank_balance' => Input::get('spouse_bank_balance'),
			'bank_tax_refund_previous_year' => Input::get('spouse_bank_tax_refund_previous_year'),
			'bank_tax_refund_amount' => Input::get('spouse_bank_tax_refund_amount'),
			'bank_retire_age' => Input::get('spouse_bank_retire_age'),
			'bank_get_back_mortage_payments' => Input::get('spouse_bank_get_back_mortage_payments')
		];

		$gps = Gps_trial_model::where('user_id', Auth::get_user_id())->first();
		$gps->spouse_bank_info = (json_encode($bank_info));
		$gps->save();
	}
}

