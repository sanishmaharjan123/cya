<?php namespace cya\clientdetails\Components;

use Input;

use cya\clientdetails\models\Gps_trial_model;

use Illuminate\Support\Facades\Auth;

use Validator;

use Redirect;

use RainLab\User\Components\Session;



class personaldetail extends \Cms\Classes\ComponentBase
{
	public function componentDetails()
	{
		return [
			'name' => 'Personal information',
			'description' => 'Edit Client Personal Information'
		];
	}

	public function onRun()
	{
		$this->informations  = Gps_trial_model::where('user_id',Auth::get_user_id())->first();
		// echo '<pre>'; print_r($this->informations); exit;
	}

	public $informations;

	public function onSave()
	{
		
		$user = Gps_trial_model::where('user_id',Auth::get_user_id())->first();
		$user->client_name = Input::get('client_name');
		$user->client_dob = Input::get('client_dob');
		$user->client_email = Input::get('client_email');
		$user->client_mobile = Input::get('client_mobile');
		$user->client_home = Input::get('client_home');
		$user->spouse_name = Input::get('spouse_name');
		$user->spouse_dob = Input::get('spouse_dob');
		$user->spouse_email = Input::get('spouse_email');
		$user->spouse_mobile = Input::get('spouse_mobile');
		$user->spouse_home = Input::get('spouse_home');

		$user->save();


	}
}