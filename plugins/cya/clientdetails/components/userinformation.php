<?php namespace cya\clientdetails\Components;

use Input;

use cya\clientdetails\models\Gps_trial_model;

use Illuminate\Support\Facades\Auth;

use Validator;

use Redirect;

use RainLab\User\Components\Session;
use RainLab\User\models\user;


use DB;


class userinformation extends \Cms\Classes\ComponentBase
{
	public function componentDetails()
	{
		return [
			'name' => 'User information',
			'description' => 'Edit Client User Information'
		];
	}

	public function onRun()
	{
		$this->informations  = DB::table('users')->where('id',Auth::get_user_id())->first();
	}

	public $informations;

	public function onSave_credential()
	{
		
		$user = User::find(Auth::get_user_id());
		$user->first_name = Input::get('first_name');
		$user->last_name = Input::get('last_name');
		$user->email = Input::get('email');
		$user->username = Input::get('username');
		if(Input::get('password') != '' || Input::get('password') != null){
			$user->password = Input::get('password');
		}
		$user->save();


	}
}
