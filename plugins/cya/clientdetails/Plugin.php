<?php namespace Cya\Clientdetails;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return [
    		'cya\clientdetails\components\gpstrial' => 'gpstrial',
    		'cya\clientdetails\components\gpstrialspouse' => 'gpstrialspouse',
            'cya\clientdetails\components\personaldetail' => 'personaldetail',
            'cya\clientdetails\components\financialinformation' => 'financialinformation',
    		'cya\clientdetails\components\userinformation' => 'userinformation',
    	];
    }

    public function registerSettings()
    {
    }
}
