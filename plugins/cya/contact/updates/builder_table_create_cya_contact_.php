<?php namespace cya\Contact\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaContact extends Migration
{
    public function up()
    {
        Schema::create('cya_contact_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('name', 255);
            $table->string('email', 255);
            $table->text('message');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_contact_');
    }
}
