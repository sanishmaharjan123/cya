<?php namespace cya\Contact;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return [
    		'cya\Contact\Components\ContactForm' => 'contactform',
    	];
    }

    public function registerSettings()
    {
    }
}
