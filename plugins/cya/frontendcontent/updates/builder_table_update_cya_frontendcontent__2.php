<?php namespace cya\FrontendContent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCyaFrontendcontent2 extends Migration
{
    public function up()
    {
        Schema::table('cya_frontendcontent_', function($table)
        {
            $table->text('background_image')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('cya_frontendcontent_', function($table)
        {
            $table->string('background_image', 255)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
