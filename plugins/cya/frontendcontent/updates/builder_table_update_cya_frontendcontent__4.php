<?php namespace cya\FrontendContent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCyaFrontendcontent4 extends Migration
{
    public function up()
    {
        Schema::table('cya_frontendcontent_', function($table)
        {
            $table->string('slug', 255);
        });
    }
    
    public function down()
    {
        Schema::table('cya_frontendcontent_', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
