<?php namespace cya\FrontendContent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCyaFrontendcontent3 extends Migration
{
    public function up()
    {
        Schema::table('cya_frontendcontent_', function($table)
        {
            $table->dropColumn('background_image');
        });
    }
    
    public function down()
    {
        Schema::table('cya_frontendcontent_', function($table)
        {
            $table->text('background_image');
        });
    }
}
