<?php namespace cya\FrontendContent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaFrontendcontent extends Migration
{
    public function up()
    {
        Schema::create('cya_frontendcontent_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('title', 255);
            $table->text('short_description');
            $table->text('body_description');
            $table->string('background_image', 255);
            $table->boolean('status');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_frontendcontent_');
    }
}
