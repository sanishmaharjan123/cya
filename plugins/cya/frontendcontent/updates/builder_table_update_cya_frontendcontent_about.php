<?php namespace cya\FrontendContent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCyaFrontendcontentAbout extends Migration
{
    public function up()
    {
        Schema::rename('cya_frontendcontent_', 'cya_frontendcontent_about');
    }
    
    public function down()
    {
        Schema::rename('cya_frontendcontent_about', 'cya_frontendcontent_');
    }
}
