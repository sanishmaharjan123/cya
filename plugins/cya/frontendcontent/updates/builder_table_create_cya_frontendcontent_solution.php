<?php namespace cya\FrontendContent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaFrontendcontentSolution extends Migration
{
    public function up()
    {
        Schema::create('cya_frontendcontent_solution', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('title', 255);
            $table->text('banner_text');
            $table->text('body');
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_frontendcontent_solution');
    }
}
