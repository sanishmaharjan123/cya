<?php namespace cya\FrontendContent\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Solution extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController','Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('cya.FrontendContent', 'main-menu-item', 'side-menu-item2');
    }
}
