<?php namespace cya\FrontendContent\Models;

use Model;

/**
 * Model
 */
class Solution_model extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    
    public $attachOne = [ 'background_image' => 'System\Models\File'    ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'cya_frontendcontent_solution';
}
