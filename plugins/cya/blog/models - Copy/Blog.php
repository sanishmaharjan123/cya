<?php namespace Cya\Blog\Models;

use Model;

/**
 * Model
 */
class Blog extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachOne = [ 'background_image' => 'System\Models\File', 'blog_image' => 'System\Models\File'  ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'cya_blog_';
}
