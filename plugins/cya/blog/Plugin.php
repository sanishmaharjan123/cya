<?php namespace Cya\Blog;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {

    	return [
    		'cya\blog\Components\Blog_comment' => 'Blog_comment',
    	];

    }

    public function registerSettings()
    {
    }
}
