<?php namespace Cya\Blog\Models;

use Model;

/**
 * Model
 */
class Blog extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsToMany = [
        'category' => [
            'Cya\Blog\Models\Category_model',
            'table' => 'cya_blog_category_blog'
        ]
    ];

 public $attachOne = [ 'background_image' => 'System\Models\File', 'blog_image' => 'System\Models\File'  ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'cya_blog_';

    public function scopeInlimit($query)
    {
        return $query->orderBy('id','desc')->limit(5);
    }
}
