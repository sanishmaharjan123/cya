<?php namespace Cya\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaBlogComments extends Migration
{
    public function up()
    {
        Schema::create('cya_blog_comments', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('blog_id');
            $table->string('name', 255);
            $table->string('email', 255);
            $table->text('comment');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_blog_comments');
    }
}
