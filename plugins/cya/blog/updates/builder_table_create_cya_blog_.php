<?php namespace Cya\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaBlog extends Migration
{
    public function up()
    {
        Schema::create('cya_blog_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('title', 255);
            $table->text('description');
            $table->string('image_link', 255);
            $table->boolean('status');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_blog_');
    }
}
