<?php namespace Cya\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaBlogCategoryBlog extends Migration
{
    public function up()
    {
        Schema::create('cya_blog_category_blog', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('blog_id');
            $table->integer('categories_id');
            $table->primary(['blog_id','categories_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_blog_category_blog');
    }
}
