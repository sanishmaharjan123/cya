<?php namespace Cya\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCyaBlog extends Migration
{
    public function up()
    {
        Schema::table('cya_blog_', function($table)
        {
            $table->text('short_description');
        });
    }
    
    public function down()
    {
        Schema::table('cya_blog_', function($table)
        {
            $table->dropColumn('short_description');
        });
    }
}
