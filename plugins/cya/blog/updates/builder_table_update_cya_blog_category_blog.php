<?php namespace Cya\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCyaBlogCategoryBlog extends Migration
{
    public function up()
    {
        Schema::table('cya_blog_category_blog', function($table)
        {
            $table->dropPrimary(['blog_id','categories_id']);
            $table->renameColumn('categories_id', 'category_model_id');
            $table->primary(['blog_id','category_model_id']);
        });
    }
    
    public function down()
    {
        Schema::table('cya_blog_category_blog', function($table)
        {
            $table->dropPrimary(['blog_id','category_model_id']);
            $table->renameColumn('category_model_id', 'categories_id');
            $table->primary(['blog_id','categories_id']);
        });
    }
}
