<?php namespace cya\blog\Components;

use Input;

use cya\blog\models\Blog_comment_model;

use Validator;

use Redirect;

class Blog_comment extends \Cms\Classes\ComponentBase
{

	public function componentDetails()
	{
		return [
			'name' => 'Blog Comments',
			'description' => ' Blog Comments Form'
		];
	}

	public function onSave()
	{
		$validator = Validator::make(
			[
				'name' => Input::get('name'),
				'email' => Input::get('email'),
				'comment' => Input::get('comment')
			] ,
			[
				'name' => 'required',
				'email' => 'required|email',
				'comment' => 'required',
			]
		);

		if($validator->fails())
		{
			return ['#result' => $this->renderPartial('Blog_comment::messages',[
				'errorMsgs' => $validator->messages()->all(),
				'fieldMsgs' => $validator->messages()
			])];

		}
		else
		{
			// save basic information
			$blog_comment = new Blog_comment_model;
			$blog_comment->blog_id = Input::get('blog_id');
			$blog_comment->name = Input::get('name');
			$blog_comment->email = Input::get('email');
			$blog_comment->comment = Input::get('comment');
			$blog_comment->save();

			return Redirect::back();
		}
	}
}

