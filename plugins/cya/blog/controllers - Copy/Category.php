<?php namespace Cya\Blog\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Category extends Controller
{
    public $implement = [
            ];
    
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Cya.Blog', 'main-menu-item', 'side-menu-item');
    }
}
