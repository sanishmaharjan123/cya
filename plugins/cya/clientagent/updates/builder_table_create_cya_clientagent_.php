<?php namespace Cya\ClientAgent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaClientagent extends Migration
{
    public function up()
    {
        Schema::create('cya_clientagent_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('client_id');
            $table->integer('adent_id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_clientagent_');
    }
}
