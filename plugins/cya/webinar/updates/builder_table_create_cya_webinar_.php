<?php namespace Cya\Webinar\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaWebinar extends Migration
{
    public function up()
    {
        Schema::create('cya_webinar_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('name', 255)->nullable();
            $table->date('date');
            $table->time('time');
            $table->boolean('status');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_webinar_');
    }
}
