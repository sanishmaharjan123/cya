<?php namespace Cya\ClientDetails;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return [
    		'cya\clientdetails\components\basicinformation' => 'basicinformation','cya\clientdetails\components\spouseinformation' => 'spouseinformation',
    	];
    }

    public function registerSettings()
    {
    }
}
