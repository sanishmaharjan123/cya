<?php namespace Cya\ClientDetails\Models;

use Model;

/**
 * Model
 */
class Spouse_information_model extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'cya_clientdetails_spouse_information';
}
