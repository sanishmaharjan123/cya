<?php namespace Cya\ClientDetails\Models;

use October\Rain\Auth\Models\User;

use Model;
use DB;

/**
 * Model
 */
class Basic_information_model extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'cya_clientdetails_basic_information';

    public $belongsToMany = [
        'users'    => ['October\Rain\Auth\Models\User', 'table' => 'cya_clientdetails_client_user']
    ];
    
    public static function get_user_details($user_id)
    {
        return DB::table('cya_clientdetails_basic_information')->where('user_id',$user_id)->get();
    }
}

