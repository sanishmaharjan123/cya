<?php namespace cya\clientdetails\Components;

use Input;

use cya\clientdetails\models\Basic_information_model;
use cya\clientdetails\models\Spouse_information_model;
use cya\clientdetails\models\Client_user;

use Illuminate\Support\Facades\Auth;

use Validator;

use Redirect;

use RainLab\User\Components\Session;



class spouseInformation extends \Cms\Classes\ComponentBase
{

	public function componentDetails()
	{
		return [
			'name' => 'Client Spouse  Details',
			'description' => ' Clinet Spouse Details Form'
		];
	}

	public function onSave()
	{
		
		$validator = Validator::make(
			[
				'name' => Input::get('name'),
				'email' => Input::get('email'),
				'dob' => Input::get('dob'),
				'mobile_no' => Input::get('mobile_no'),
				'second_no' => Input::get('second_no')
			] ,
			[
				'name' => 'required',
				'email' => 'required|email',
				'dob' => 'required',
				'mobile_no' => 'required|digits:10',
				'second_no' => 'required',
			]
		);

		if($validator->fails())
		{
			return ['#result' => $this->renderPartial('basic_Information::messages',[
				'errorMsgs' => $validator->messages()->all(),
				'fieldMsgs' => $validator->messages()
			])];

		}
		else
		{
			// save basic information
			$spouse_information = new Spouse_information_model;
			$spouse_information->name = Input::get('name');
			$spouse_information->dob = Input::get('dob');
			$spouse_information->email = Input::get('email');
			$spouse_information->mobile_no = Input::get('mobile_no');
			$spouse_information->second_no = Input::get('second_no');
			$spouse_information->user_id = Auth::get_user_id();
			$spouse_information->save();

			$spouse = Basic_information_model::where('user_id', Auth::get_user_id())->first();
			$spouse->spouse_id = $spouse_information->id;
			$spouse->save();
		}
	}

	public function onSave_revolving()
	{
		// save revolving accounts
		$count_rev_count = count(Input::get('rev_account_balance'));
		$rev_account_balance = Input::get('rev_account_balance');
		$rev_interest_rate = Input::get('rev_interest_rate');
		$rev_minimum_payment = Input::get('rev_minimum_payment');
		$rev_actual_pay = Input::get('rev_actual_pay');
		$rev_due_date = Input::get('rev_due_date');
		$rev_spouse_id = Input::get('rev_spouse_id');
		for ($i=0; $i < $count_rev_count; $i++) 
		{ 
			$revolving_account[$i] = [
				'rev_account_balance'=> $rev_account_balance[$i],
				'rev_interest_rate'=> $rev_interest_rate[$i],
				'rev_minimum_payment'=> $rev_minimum_payment[$i],
				'rev_actual_pay'=> $rev_actual_pay[$i],
				'rev_due_date'=> $rev_due_date[$i]
			];
		}

		$gps = Gps_trial_model::where('user_id', Auth::get_user_id())->first();
		$gps->revolving_account = (json_encode($revolving_account));
		$gps->save();

	}

	public function onSave_installment()
	{
			// save installment section
		$inst_installment_account = Input::get('inst_installment_account');
		$inst_interest_rate = Input::get('inst_interest_rate');
		$inst_payment = Input::get('inst_payment');
		$inst_due_date = Input::get('inst_due_date');

		$count_installment = count($inst_installment_account);
		for ($i=0; $i < $count_installment; $i++) 
		{ 
			$installment[$i] = [
				'inst_installment_account'=> $inst_installment_account[$i],
				'inst_interest_rate'=> $inst_interest_rate[$i],
				'inst_payment'=> $inst_payment[$i],
				'inst_due_date'=> $inst_due_date[$i]
			];
		}
		$gps = Gps_trial_model::where('user_id', Auth::get_user_id())->first();
		$gps->installment = (json_encode($installment));
		$gps->save();
	}

	public function onSave_mortgage()
	{

			// save mortgage
		$mor_mortgage_type = Input::get('mor_mortgage_type');
		$mor_payment = Input::get('mor_payment');
		$mor_interest_rate = Input::get('mor_interest_rate');
		$mor_balance = Input::get('mor_balance');
		$mor_purchase_price = Input::get('mor_purchase_price');
		$mor_estimated_value = Input::get('mor_estimated_value');

		$count_mortgage = count($mor_mortgage_type);
		for ($i=0; $i < $count_mortgage; $i++) 
		{ 
			$mortgage[$i] = [
				'mor_mortgage_type'=> $mor_mortgage_type[$i],
				'mor_payment'=> $mor_payment[$i],
				'mor_interest_rate'=> $mor_interest_rate[$i],
				'mor_balance'=> $mor_balance[$i],
				'mor_purchase_price'=> $mor_purchase_price[$i],
				'mor_estimated_value'=> $mor_estimated_value[$i]
			];
		}
		$gps = Gps_trial_model::where('user_id', Auth::get_user_id())->first();
		$gps->mortgage = (json_encode($mortgage));
		$gps->save();

	}
	public function onSave_expenses()
	{
			// save expense
		$inhouse_expense_name = Input::get('inhouse_expense_name');
		$inhouse_other_expenses = Input::get('inhouse_other_expenses');

		$overall_expense_name = Input::get('overall_expense_name');
		$overall_other_expenses = Input::get('overall_other_expenses');

		$count_inhouse = count($inhouse_expense_name);
		for ($i=0; $i < $count_inhouse; $i++) 
		{ 
			$inhouse_expense[$i] = [
				'inhouse_expense_name'=> $inhouse_expense_name[$i],
				'inhouse_other_expenses'=> $inhouse_other_expenses[$i]
			];
		}

		$count_overall = count($overall_expense_name);
		for ($i=0; $i < $count_overall; $i++) 
		{ 
			$overall_expense[$i] = [
				'overall_expense_name'=> $overall_expense_name[$i],
				'overall_other_expenses'=> $overall_other_expenses[$i],
			];
		}

		$expense = [
			'exp_groceries' => Input::get('inhouse_groceries'),
			'exp_cable' => Input::get('inhouse_cable'),
			'exp_cellphone' => Input::get('inhouse_cellphone'),
			'exp_utilities' => Input::get('inhouse_utilities'),
			'exp_clothes' => Input::get('inhouse_clothes'),
			'exp_dry_cleaning' => Input::get('inhouse_dry_cleaning'),
			'exp_entertainment' => Input::get('inhouse_entertainment'),
			'exp_emergency' => Input::get('inhouse_emergency'),
			'exp_emergency' => Input::get('inhouse_emergency'),
			'inhouse_other_expenses' => json_encode($inhouse_expense),
			'inhouse_total' => Input::get('inhouse_total'),
			'exp_grooming' => Input::get('overall_grooming'),
			'exp_tolls' => Input::get('overall_tolls'),
			'exp_eating_out' => Input::get('overall_eating_out'),
			'exp_gas' => Input::get('overall_gas'),
			'exp_children_lesso' => Input::get('overall_children_lesso'),
			'exp_daycare' => Input::get('overall_daycare'),
			'exp_hair_nails' => Input::get('overall_hair_nails'),
			'exp_gym' => Input::get('overall_gym'),
			'overall_expenses' => json_encode($overall_expense),
			'overall_total' => Input::get('overall_total')
		];

		$gps = Gps_trial_model::where('user_id', Auth::get_user_id())->first();
		$gps->expense = (json_encode($expense));
		$gps->save();
	}
	public function onSave_income()
	{		
			// save income
		$inc_pay_frequency = Input::get('inc_pay_frequency');
		$inc_net_income = Input::get('inc_net_income');
		$inc_extra_income = Input::get('inc_extra_income');
		$inc_pay_day = Input::get('inc_pay_day');

		$count_income = count($inc_pay_frequency);
		for ($i=0; $i < $count_income; $i++) 
		{ 
			$income[$i] = [
				'inc_pay_frequency'=> $inc_pay_frequency[$i],
				'inc_net_income'=> $inc_net_income[$i],
				'inc_extra_income'=> $inc_extra_income[$i],
				'inc_pay_day'=> $inc_pay_day[$i],
			];
		}
		$gps = Gps_trial_model::where('user_id', Auth::get_user_id())->first();
		$gps->income = (json_encode($income));
		$gps->save();
	}

	public function onSave_bank_info()
	{
			// save bank info
		$bank_info = [
			'bank_checking_account_balance' => Input::get('bank_checking_account_balance'),
			'bank_saving_account_balance' => Input::get('bank_saving_account_balance'),
			'bank_pretax_saving_plan' => Input::get('bank_pretax_saving_plan'),
			'bank_contribution_per_day' => Input::get('bank_contribution_per_day'),
			'bank_get_match' => Input::get('bank_get_match'),
			'bank_balance' => Input::get('bank_balance'),
			'bank_tax_refund_previous_year' => Input::get('bank_tax_refund_previous_year'),
			'bank_tax_refund_amount' => Input::get('bank_tax_refund_amount'),
			'bank_retire_age' => Input::get('bank_retire_age'),
			'bank_get_back_mortage_payments' => Input::get('bank_get_back_mortage_payments')
		];

		$gps = Gps_trial_model::where('user_id', Auth::get_user_id())->first();
		$gps->bank_info = (json_encode($bank_info));
		$gps->save();
	}
}

