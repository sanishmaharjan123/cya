<?php namespace Cya\ClientDetails\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Client extends Controller
{
    public $implement = [
        'Backend\Behaviors\ReorderController'    ];
    
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Cya.ClientDetails', 'main-menu-item');
    }
}
