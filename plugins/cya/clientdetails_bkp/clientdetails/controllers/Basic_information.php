<?php namespace Cya\ClientDetails\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Basic_information extends Controller
{
    public $implement = [
        'Backend\Behaviors\ReorderController'    ];
    
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
    }
}
