<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCyaClientdetailsMortgage extends Migration
{
    public function up()
    {
        Schema::table('cya_clientdetails_mortgage', function($table)
        {
            $table->integer('mor_user_id');
        });
    }
    
    public function down()
    {
        Schema::table('cya_clientdetails_mortgage', function($table)
        {
            $table->dropColumn('mor_user_id');
        });
    }
}
