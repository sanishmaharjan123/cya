<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCyaClientdetailsClientUser2 extends Migration
{
    public function up()
    {
        Schema::table('cya_clientdetails_client_user', function($table)
        {
            $table->dropPrimary(['id','user_id']);
            $table->renameColumn('id', 'basic_information_model_user_id');
            $table->primary(['user_id']);
        });
    }
    
    public function down()
    {
        Schema::table('cya_clientdetails_client_user', function($table)
        {
            $table->dropPrimary(['user_id']);
            $table->renameColumn('basic_information_model_user_id', 'id');
            $table->primary(['id','user_id']);
        });
    }
}
