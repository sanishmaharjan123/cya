<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCyaClientdetailsBankInfo extends Migration
{
    public function up()
    {
        Schema::table('cya_clientdetails_bank_info', function($table)
        {
            $table->integer('bank_user_id');
        });
    }
    
    public function down()
    {
        Schema::table('cya_clientdetails_bank_info', function($table)
        {
            $table->dropColumn('bank_user_id');
        });
    }
}
