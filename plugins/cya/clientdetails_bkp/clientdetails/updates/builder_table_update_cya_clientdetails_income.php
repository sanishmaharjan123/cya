<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCyaClientdetailsIncome extends Migration
{
    public function up()
    {
        Schema::table('cya_clientdetails_income', function($table)
        {
            $table->integer('inc_user_id');
        });
    }
    
    public function down()
    {
        Schema::table('cya_clientdetails_income', function($table)
        {
            $table->dropColumn('inc_user_id');
        });
    }
}
