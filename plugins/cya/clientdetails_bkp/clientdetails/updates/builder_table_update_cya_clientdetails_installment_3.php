<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCyaClientdetailsInstallment3 extends Migration
{
    public function up()
    {
        Schema::table('cya_clientdetails_installment', function($table)
        {
            $table->string('inst_other_installment', 255);
        });
    }
    
    public function down()
    {
        Schema::table('cya_clientdetails_installment', function($table)
        {
            $table->dropColumn('inst_other_installment');
        });
    }
}
