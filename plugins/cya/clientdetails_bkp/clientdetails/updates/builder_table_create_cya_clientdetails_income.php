<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaClientdetailsIncome extends Migration
{
    public function up()
    {
        Schema::create('cya_clientdetails_income', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('inc_pay_frequency', 255);
            $table->double('inc_net_income', 10, 0);
            $table->double('inc_extra_income', 10, 0);
            $table->integer('inc_pay_day');
            $table->integer('inc_spouse_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_clientdetails_income');
    }
}
