<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCyaClientdetailsInstallment2 extends Migration
{
    public function up()
    {
        Schema::table('cya_clientdetails_installment', function($table)
        {
            $table->integer('inst_user_id');
        });
    }
    
    public function down()
    {
        Schema::table('cya_clientdetails_installment', function($table)
        {
            $table->dropColumn('inst_user_id');
        });
    }
}
