<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaClientdetailsInstallmentSection extends Migration
{
    public function up()
    {
        Schema::create('cya_clientdetails_installment_section', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('inst_installment_account', 255);
            $table->double('inst_interest_rate', 10, 0);
            $table->double('inst_payment', 10, 0);
            $table->integer('inst_due_date');
            $table->integer('inst_spouse_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_clientdetails_installment_section');
    }
}
