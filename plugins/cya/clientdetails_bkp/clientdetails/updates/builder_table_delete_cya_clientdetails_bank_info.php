<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteCyaClientdetailsBankInfo extends Migration
{
    public function up()
    {
        Schema::dropIfExists('cya_clientdetails_bank_info');
    }
    
    public function down()
    {
        Schema::create('cya_clientdetails_bank_info', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->double('bank_checking_account_balance', 10, 0);
            $table->double('bank_saving_account_balance', 10, 0);
            $table->string('bank_pretax_saving_plan', 255);
            $table->double('bank_contribution_per_day', 10, 0);
            $table->string('bank_get_match', 255);
            $table->double('bank_balance', 10, 0);
            $table->string('bank_tax_refund_previous_year', 255);
            $table->double('bank_tax_refund_amount', 10, 0);
            $table->integer('bank_retire_age');
            $table->string('bank_get_back_mortage_payments', 255);
            $table->integer('bank_spouse_id');
            $table->integer('bank_user_id');
        });
    }
}
