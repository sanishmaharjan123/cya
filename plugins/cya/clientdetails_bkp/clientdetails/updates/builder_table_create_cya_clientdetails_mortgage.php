<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaClientdetailsMortgage extends Migration
{
    public function up()
    {
        Schema::create('cya_clientdetails_mortgage', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('mor_mortgage_type', 255);
            $table->double('mor_payment', 10, 0);
            $table->double('mor_interest_rate', 10, 0);
            $table->double('mor_balance', 10, 0);
            $table->double('mor_purchase_price', 10, 0);
            $table->double('mor_estimated_value', 10, 0);
            $table->integer('mor_spouse_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_clientdetails_mortgage');
    }
}
