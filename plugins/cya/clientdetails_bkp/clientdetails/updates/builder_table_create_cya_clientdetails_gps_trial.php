<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaClientdetailsGpsTrial extends Migration
{
    public function up()
    {
        Schema::create('cya_clientdetails_gps_trial', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('user_id');
            $table->text('revolving_account');
            $table->text('installment');
            $table->text('mortgage');
            $table->text('income');
            $table->text('bank_info');
            $table->text('expense');
            $table->integer('spouse_id')->nullable();
            $table->integer('version');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_clientdetails_gps_trial');
    }
}
