<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaClientdetailsClientUser extends Migration
{
    public function up()
    {
        Schema::create('cya_clientdetails_client_user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('user_id');
            $table->integer('client_user_id');
            $table->primary(['user_id','client_user_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_clientdetails_client_user');
    }
}
