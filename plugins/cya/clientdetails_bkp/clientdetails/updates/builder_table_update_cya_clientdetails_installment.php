<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCyaClientdetailsInstallment extends Migration
{
    public function up()
    {
        Schema::rename('cya_clientdetails_installment_section', 'cya_clientdetails_installment');
    }
    
    public function down()
    {
        Schema::rename('cya_clientdetails_installment', 'cya_clientdetails_installment_section');
    }
}
