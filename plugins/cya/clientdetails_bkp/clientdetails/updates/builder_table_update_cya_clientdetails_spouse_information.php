<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCyaClientdetailsSpouseInformation extends Migration
{
    public function up()
    {
        Schema::table('cya_clientdetails_spouse_information', function($table)
        {
            $table->string('name', 255);
            $table->string('dob', 255);
            $table->string('email', 255);
            $table->string('mobile_no', 255);
            $table->string('second_no', 255);
        });
    }
    
    public function down()
    {
        Schema::table('cya_clientdetails_spouse_information', function($table)
        {
            $table->dropColumn('name');
            $table->dropColumn('dob');
            $table->dropColumn('email');
            $table->dropColumn('mobile_no');
            $table->dropColumn('second_no');
        });
    }
}
