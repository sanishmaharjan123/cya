<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaClientdetailsRevolvingAccounts extends Migration
{
    public function up()
    {
        Schema::create('cya_clientdetails_revolving_accounts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('rev_user_id');
            $table->double('rev_account_balance', 10, 0)->default(0);
            $table->double('rev_interest_rate', 10, 0)->default(0);
            $table->double('rev_minimum_payment', 10, 0)->default(0);
            $table->double('rev_actual_pay', 10, 0)->default(0);
            $table->integer('rev_due_date');
            $table->integer('rev_spouse_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_clientdetails_revolving_accounts');
    }
}
