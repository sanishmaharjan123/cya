<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCyaClientdetailsBasicInformation extends Migration
{
    public function up()
    {
        Schema::table('cya_clientdetails_basic_information', function($table)
        {
            $table->integer('spouse_id');
        });
    }
    
    public function down()
    {
        Schema::table('cya_clientdetails_basic_information', function($table)
        {
            $table->dropColumn('spouse_id');
        });
    }
}
