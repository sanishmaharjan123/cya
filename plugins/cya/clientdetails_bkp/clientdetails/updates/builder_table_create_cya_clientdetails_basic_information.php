<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaClientdetailsBasicInformation extends Migration
{
    public function up()
    {
        Schema::create('cya_clientdetails_basic_information', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('name', 255);
            $table->date('dob');
            $table->string('email', 255);
            $table->string('mobile_no', 255);
            $table->string('second_no', 255);
            $table->integer('user_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_clientdetails_basic_information');
    }
}
