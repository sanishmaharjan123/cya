<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteCyaClientdetailsClientUser extends Migration
{
    public function up()
    {
        Schema::dropIfExists('cya_clientdetails_client_user');
    }
    
    public function down()
    {
        Schema::create('cya_clientdetails_client_user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('user_id');
            $table->integer('basic_information_model_id');
            $table->primary(['user_id']);
        });
    }
}
