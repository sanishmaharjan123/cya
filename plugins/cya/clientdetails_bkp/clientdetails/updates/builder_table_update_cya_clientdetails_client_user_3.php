<?php namespace Cya\ClientDetails\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCyaClientdetailsClientUser3 extends Migration
{
    public function up()
    {
        Schema::table('cya_clientdetails_client_user', function($table)
        {
            $table->renameColumn('basic_information_model_user_id', 'basic_information_model_id');
        });
    }
    
    public function down()
    {
        Schema::table('cya_clientdetails_client_user', function($table)
        {
            $table->renameColumn('basic_information_model_id', 'basic_information_model_user_id');
        });
    }
}
