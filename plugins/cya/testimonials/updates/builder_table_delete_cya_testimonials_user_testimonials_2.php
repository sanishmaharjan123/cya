<?php namespace Cya\Testimonials\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteCyaTestimonialsUserTestimonials2 extends Migration
{
    public function up()
    {
        Schema::dropIfExists('cya_testimonials_user_testimonials');
    }
    
    public function down()
    {
        Schema::create('cya_testimonials_user_testimonials', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('user_id');
            $table->integer('testimonial_id');
        });
    }
}
