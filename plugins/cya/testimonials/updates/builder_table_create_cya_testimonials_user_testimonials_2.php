<?php namespace Cya\Testimonials\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaTestimonialsUserTestimonials2 extends Migration
{
    public function up()
    {
        Schema::create('cya_testimonials_user_testimonials', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('testimonial_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_testimonials_user_testimonials');
    }
}
