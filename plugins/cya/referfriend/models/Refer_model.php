<?php namespace Cya\ReferFriend\Models;

use Model;

/**
 * Model
 */
class Refer_model extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;
    public $testimonial_form;
    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'cya_referfriend_';

    protected $fillable = array(
        'referer_name',
        'refer_type',
        'referee_name',
        'referee_email',
    );
}
