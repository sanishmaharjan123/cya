<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/multi-select.css') ?>">
<section class="content-header">
    <h1>
        Add Content Type
        <small></small>                    
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php  echo site_url('admin')?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php  echo site_url('vacancydatum/admin/vacancydatum')?>">vacancydatum</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <button class="btn btn-primary" onclick="toogleoption('post')">Vacancy</button>
        <button class="btn btn-primary" onclick="toogleoption('form_option')">Form Option</button>
        <button class="btn btn-primary" onclick="toogleoption('scope')">Scope Option</button>
        <!-- form start -->
        <form role="form" action="<?php echo site_url('vacancydatum/admin/vacancydatum/save')?>" method="post">
            <div class="box-body">
                <div id="vacancy_data">
                    <div class="form-group">
                        <label><?=lang('preferred_post')?></label><input name="preferred_post" id="preferred_post" class="form-control" value="<?php echo set_value('preferred_post')?>"/></div><div class="form-group">
                        <label><?=lang('specialized_area')?></label><input name="specialized_area" id="specialized_area" class="form-control" value="<?php echo set_value('specialized_area')?>"/></div><div class="form-group">
                        <label><?=lang('vacancy_code')?></label><input name="vacancy_code" id="vacancy_code" class="form-control" value="<?php echo set_value('vacancy_code')?>"/></div>
                        <div class="form-group"><label>Description</label><textarea class="redactor" name="description" id="description"></textarea></div>
                        <div class="form-group"><label>Start Date</label><input type="date" name="vacancy_start_date" id="vacancy_start_date" class="form-control" value="<?php echo set_value('vacancy_start_date')?>"/></div>
                        <div class="form-group"><label>End Date</label><input type="date" name="vacancy_end_date" id="vacancy_end_date" class="form-control" value="<?php echo set_value('vacancy_end_date')?>"/></div>
                        <input type="hidden" name="id" id="id"/>
                    </div><!-- /.box-body -->
                </div>
                <div class="box-body">
                    <div id="form_data" style="display: none;">
                        <h4>Education</h4>
                        <div class="form-group">
                            <label>Master</label>
                            <div class="radio"><label><input type="radio" value="1" name="master" />Required</label> 
                            <label><input type="radio" value="0" name="master" />Not Required</label></div>
                        </div>
                        <div class="form-group">
                            <label>Bachelor</label>
                            <div class="radio"><label><input type="radio" value="1" name="bachelor" />Required</label> 
                            <label><input type="radio" value="0" name="bachelor" />Not Required</label></div>
                        </div>
                        <h4>Work Experience</h4>
                        <div class="form-group">
                            <label>Experience</label>
                            <div class="radio"><label><input type="radio" value="1" name="experience" />Required</label> 
                            <label><input type="radio" value="0" name="experience" />Not Required</label></div>
                        </div>
                    </div>    
                </div>
                <div class="box-body">
                    <div id="scope_data" style="display: none;">
                        <h4>Add As Many Zone And District</h4>
                        <div id="input">
                            <div class="form-group">
                                <label>Select Zone</label>
                                <select class="form-control" name="zone[]" onchange="getDistrict(1)" id="zone_option_1">
                                  <?php foreach ($zones as $zone) { ?>
                                    <option value="<?php echo $zone['z_id'] ?>"><?php echo $zone['zone'] ?></option>
                                  <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Select Zone</label>
                                <select multiple="multiple" class="form-control district-select" onchange ="show_branches(1)" name="district[]" id="district_option_1">
                                
                                </select>
                            </div>
                            <div id="all_branches_1">
                    
                            </div>
                        </div> 
                        
                        <div class="form-group">
                            <button type="button" id="add-zone-option" class="btn btn-flat btn-success">Add More</button>
                            <button type="button" id="remove-zone-option" class="btn btn-flat btn-warning">Remove</button>
                        </div>
                        
                    </div>    
                </div>
                
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="<?php  echo site_url('vacancydatum/admin/vacancydatum')?>" class="btn btn-danger">Cancel</a>
            </div>
        </form>
    </div>    

</section> 

<script type="text/javascript" src="<?php echo base_url('assets/js/multi-select.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#remove-zone-option').attr('disabled',true);
        $('.district-select').multiSelect();
    });
    var globel = 1;
    function toogleoption(option)
    {
        if(option == 'post')
        {
            $('#vacancy_data').show();
            $('#scope_data').hide();                            
            $('#form_data').hide();

        }
        else if(option == 'scope')
        {
            $('#vacancy_data').hide();
            $('#form_data').hide();
            $('#scope_data').show();
        }
        else
        {
            $('#scope_data').hide();
            $('#vacancy_data').hide();
            $('#form_data').show();

        }
    }

    $('#add-zone-option').click(function(){
        var id = globel;
        id++;
        globel++;
        $('#input').after('<div id="input" class="clonedInput">'+
                            '<div class="form-group">'+
                                '<label>Select Zone</label>'+
                                '<select class="form-control" name="zone[]" onchange="getDistrict('+id+')" id="zone_option_'+id+'">'+
                                  '<?php foreach ($zones as $zone) { ?>'+
                                    '<option value="<?php echo $zone['z_id'] ?>"><?php echo $zone['zone'] ?></option>'+
                                  '<?php } ?>'+
                                '</select>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label>Select Zone</label>'+ 
                                '<select multiple="multiple" class="form-control district-select" onchange="show_branches('+id+')" name="district[]" id="district_option_'+id+'">'+
                                    
                                '</select>'+
                            '</div>'+
                            '<div id="all_branches_'+id+'">'+
                        '</div>'+
                        
                    
                        '</div>'
                        )
        $('#remove-zone-option').attr('disabled',false);
    });
    $('#remove-zone-option').click(function(){
        $('.clonedInput').last().remove();
        if(globel > 1)
        {
            globel--;
        }

    });

    function getDistrict(index)
    {
        zone = $('#zone_option_'+index).val();
        $.post("<?php echo site_url('vacancydatum/admin/vacancydatum/getDistrict') ?>", {zone:zone}, function(result){
            $('#district_option_'+index).empty();
           $.each(result.districts,function(key,value){
                $('#district_option_'+index).append('<option value="'+value.zone_id+'_'+value.d_id+'">'+value.district+'</option>');
           });
        },'json');
    }

    function show_branches(index){
        district = $('#district_option_'+index).val();        
        $.post("<?php echo site_url('vacancydatum/admin/vacancydatum/getBranches') ?>", {district:district}, function(result){
            $('#all_branches_'+index).empty(); 
            $.each(result.branches,function(key,value){
                $('#all_branches_'+index).append('<input type="checkbox" name="branch_id[]" value="'+value.id+'">'+value.name);   
            });
        },'json');
    }
</script>