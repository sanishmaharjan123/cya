<?php namespace cya\Referfriend\Components;


use Input;
use Mail;
use cya\Referfriend\models\Refer_model;
use Flash;
class Referform extends \Cms\Classes\ComponentBase
{

	public function componentDetails()
	{
		return [
			'name' => 'Referel Friend Form',
			'description' => ' Simple Referel form'
		];
	}

	public function onRun()
	{
		$this->page['refers'] = $this->testimonials = Refer_model::all();
	}

	public function onSave()
	{
		$refer = new Refer_model;
		$refer->referer_name = Input::get('full_name');
		$refer->referee_name = Input::get('friend_full_name');
		$refer->referee_email = Input::get('friend_email');
		$refer->refer_type = Input::get('type');
		$refer->save();

		$vars = ['full_name' => Input::get('full_name'), 'friend_fullname'=> Input::get('friend_full_name')];
		
		if(Input::get('type') == 'website_refer')
			{

				Mail::send('cya.referfriend::mail.message', $vars, function($message) {
					$message->to(Input::get('friend_email'), Input::get('friend_full_name'));
				});
			}
			else
			{
				Mail::send('cya.referfriend::mail.gps-trial', $vars, function($message) {
					$message->to(Input::get('friend_email'), Input::get('friend_full_name'));
				});
			}

			
		}
	}

