<?php namespace Cya\ReferFriend;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return [
    		'cya\ReferFriend\Components\Referform' => 'referform',
    	];

    }

    public function registerSettings()
    {
    }
}
