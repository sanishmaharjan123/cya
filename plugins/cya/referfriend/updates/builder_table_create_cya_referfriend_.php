<?php namespace Cya\ReferFriend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaReferfriend extends Migration
{
    public function up()
    {
        Schema::create('cya_referfriend_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('referer_name', 255)->nullable();
            $table->string('referee_name', 255)->nullable();
            $table->string('referee_email', 255);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('refer_type', 255);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_referfriend_');
    }
}
