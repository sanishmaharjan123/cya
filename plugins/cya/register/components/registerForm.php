<?php namespace cya\registerForm\Components;

use Input;
use cya\register\models\Register;

class registerForm extends \Cms\Classes\ComponentBase
{

	public function componentDetails()
	{
		return [
			'name' => 'Register Form',
			'description' => ' Simple Register Form'
		];
	}

	public function onSend()
	{
		$register = new Register;
		$register->first_name = Input::get('first_name');
		$register->last_name = Input::get('last_name');
		$register->email = Input::get('email');
		$register->password = Hash::make(Input::get('password'));
		$register->save();
	}


}