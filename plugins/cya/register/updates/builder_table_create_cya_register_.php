<?php namespace cya\Register\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaRegister extends Migration
{
    public function up()
    {
        Schema::create('cya_register_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('first_name', 255);
            $table->string('last_name', 255);
            $table->string('email', 255);
            $table->string('password', 255);
            $table->boolean('is_verified')->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_register_');
    }
}
