<?php namespace Cya\AddVideos\Models;

use Model;

/**
 * Model
 */
class Add_video_model extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'cya_addvideos_';

   

    public function scopeIsFeatured($query)
    {
        return $query->where('featured',1)->orderBy('updated_at','desc')->limit(1);
    }
}
