<?php namespace Cya\AddVideos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCyaAddvideos extends Migration
{
    public function up()
    {
        Schema::create('cya_addvideos_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('video_id', 255);
            $table->string('type', 255);
            $table->smallInteger('featured')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('cya_addvideos_');
    }
}
