<!DOCTYPE html>
<html>
<head>

	<title>CYA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- bootstrap -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<script type="text/javascript" src="js/bootstrap.js"></script>

	<!-- put at last -->
	<link rel="stylesheet" type="text/css" href="css/global.css">
	<link rel="stylesheet" type="text/css" href="css/styles.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

    <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700,900" rel="stylesheet">
</head>
<body>
	<div class="wrapper" id="wrapper">
		<div class="header-wrapper" id="header-wrapper">
            <div class="custom-container">
                <div class="header">
                    <div class="logo">
                        <div class="logo-field">
                            <a href="index.php"> <img src="img/logo.png" alt="logo"></a>
                        </div>
                    </div>
                    <div class="header-nav ease">
                        <ul class="navigate">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="about.php">About</a></li>
                            <li><a href="webinar.php">Webinar Schedule</a></li>
                            <li><a href="login.php">GPS Trial</a></li>
                            <li><a href="contact.php">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="mobile-nav navbar-button" title="Navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </div>
                    <div class="mainmenu"></div>
                </div>
            </div>
		</div>
		<div class="content-wrapper" id="content-wrapper">
			