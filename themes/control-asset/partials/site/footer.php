
</div> <!--CONTENT WRAPPER ENDS-->

        <div id="footer-wrapper" class="footer-wrapper">
            <div class="common-container">
                <div class="footer">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-block">
                                <div class="title">
                                    <h3>CONTACT INFO</h3>
                                </div>
                                <div class="footer-links">
                                    <ul class="foot-linkers">
                                        <li>Phone: (713) 300-1760</li>
                                        <li>Email: info@hireyourhouse.com</li>
                                        <li>Support: support@cya411.com</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="footer-block">
                                <div class="title">
                                    <h3>QUICK LINKS</h3>
                                </div>
                                <div class="footer-links">
                                    <ul class="foot-linkers">
                                        <li><a href="index.php">Home</a></li>
                                        <li><a href="about.php">About Us</a></li>
                                        <li><a href="webinar.php">Webniar Schedule</a></li>
                                        <li><a href="author.php">Author</a></li>
                                        <li><a href="contact.php">Contact</a></li>
                                        <li><a href="refer.php">Refer a friend</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-12">
                            <div class="footer-block">
                                <div class="title">
                                    <h3>ACCOUNTS</h3>
                                </div>
                                <div class="footer-links">
                                    <ul class="foot-linkers">
                                        <li><a href="blog.php">Blog</a></li>
                                        <li><a href="login.php">Sign Up</a></li>
                                        <li><a href="help.php">Help & Support</a></li>
                                        <li><a href="legal.php">Legal</a></li>
                                        <li><a href="terms.php">Terms of Service</a></li>
                                        <li><a href="news.php">In The News</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div> <!--CONTAINER-->
        
        </div> <!--FOOTER WRAPPER ENDS-->
        <div class="social-media">
            <ul class="social-linkers">
                <li>
                    <a href="https://www.facebook.com/hireyourhouse/?fref=ts">
                        <div>
                            <i class="fa fa-facebook-official" aria-hidden="true"></i>
                        </div>
                        <div>Facebook</div>
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/hireyourhouse">
                        <div>
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </div>
                    <div>Twitter</div>
                    </a>
                </li>
                <li>
                    <a href="contact.php">
                        <div>
                            <i class="fa fa-phone" aria-hidden="true"></i>
                        </div>
                        <div>Contact Us</div>
                    </a>
                </li>
                <li>
                    <a href="contact.php">
                        <div>
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        </div>
                        <div>Mail Us</div>
                    </a>
                </li>
            </ul>
        </div>
    
    
</div> <!--WRAPPER ENDS-->
<!--<script src="/js/jquery.js"></script>-->
<script type="text/javascript" src="js/jquery.mCustomScrollbar.min.js"></script>


<script>
//    var $ = jQuery;
$('.mobile-nav').on('click', function () {
    $(this).toggleClass('open');
    $('.ease').toggleClass('active');
});

$(document).ready(function(){
    mcustomscroll();
});


// functions

function mcustomscroll() {
    $('.detail-content').mCustomScrollbar();
    $('.table-container').mCustomScrollbar({
        axis: "x"
    });

}

</script>

</body>
</html>