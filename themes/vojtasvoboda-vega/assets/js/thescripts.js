var $ = jQuery;



$(document).ready(function () {
    slickInit();
    classChange();
});


function slickInit(){
    $('.free-thumb-slider').slick({
        autoplay: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        dots: true
    });


    $('.video-main-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.video-thumb-slider'
    });
    
    $('.video-thumb-slider').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      asNavFor: '.video-main-slider',
      dots: true,
      focusOnSelect: true,            
        responsive: [
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: true
              }
            },
          ]
    });

}

function classChange(){
		$(".record-list .video-item ").click(function() {
  		console.log('yo')
	});

/*	$(".record-list .video-item iframe").click(function() {
  		console.log('yo')
	});*/
}

